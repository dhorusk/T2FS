#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "t2fs_helper.h"

int main() {
	FILE2 f = open2("/etc_dir/LongFile");
	dumpOpenFiles();

	MFT_REGISTER mft_reg = mftRegisterFromMftNumber(7);
	dumpMftRegisterInfo(&mft_reg);

	mft_reg = mftRegisterFromMftNumber(8);
	dumpMftRegisterInfo(&mft_reg);

	int ret = seek2(f, 10000);
	printf("seek returned: %d\n", ret);
	ret = truncate2(f);
	printf("truncate returned: %d\n", ret);
	dumpOpenFiles();

	mft_reg = mftRegisterFromMftNumber(7);
	dumpMftRegisterInfo(&mft_reg);

	mft_reg = mftRegisterFromMftNumber(8);
	dumpMftRegisterInfo(&mft_reg);

	ret = seek2(f, 1025);
	printf("seek returned: %d\n", ret);
	ret = truncate2(f);
	printf("truncate returned: %d\n", ret);
	dumpOpenFiles();

	mft_reg = mftRegisterFromMftNumber(7);
	dumpMftRegisterInfo(&mft_reg);

	mft_reg = mftRegisterFromMftNumber(8);
	dumpMftRegisterInfo(&mft_reg);

	ret = seek2(f, 1);
	printf("seek returned: %d\n", ret);
	ret = truncate2(f);
	printf("truncate returned: %d\n", ret);
	dumpOpenFiles();

	mft_reg = mftRegisterFromMftNumber(7);
	dumpMftRegisterInfo(&mft_reg);

	mft_reg = mftRegisterFromMftNumber(8);
	dumpMftRegisterInfo(&mft_reg);

	ret = seek2(f, 0);
	printf("seek returned: %d\n", ret);
	ret = truncate2(f);
	printf("truncate returned: %d\n", ret);
	dumpOpenFiles();

	mft_reg = mftRegisterFromMftNumber(7);
	dumpMftRegisterInfo(&mft_reg);

	mft_reg = mftRegisterFromMftNumber(8);
	dumpMftRegisterInfo(&mft_reg);

    return 0;
}
