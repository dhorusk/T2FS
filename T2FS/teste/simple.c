#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "t2fs_helper.h"
#include "apidisk.h"

int main() {
	
	int r = create2("/file5.dat");
	printf("\n create resp %d \n", r);
	dumpOpenFiles();
	
	int dir = mkdir2("/srt");
	printf("\n mkdir resp %d \n", dir);
	dumpOpenDirs();

    return 0;
}
