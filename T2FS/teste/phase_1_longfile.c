#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "t2fs_helper.h"
#include "apidisk.h"

void read_dir(DIR2 dh, char* path, int path_length) {
    int ret;
    FILE2 fh;
    
    DIRENT2 dentry;
    
    printf("We are reading a directory...\n");
    
	do {
		printf("ret = readdir2(dh, &dentry) -> ");
		ret = readdir2(dh, &dentry);
		printf("returned %d\n", ret);
        
        if (dentry.fileType == TYPEVAL_REGULAR) {
            printf("We are reading a file...\n");
            char* filepath;
            
            int file_name_length = strlen(dentry.name);
            int fullpath_length = path_length + file_name_length;
            
            filepath = malloc(fullpath_length * sizeof(char));
            
            strcpy(filepath, path);
            strcat(filepath, "/");
            strcat(filepath, dentry.name);

            printf("fh = open2(\"%s\") -> ", filepath);
            fh = open2(filepath);
            printf("returned %d\n", fh);

            char buf[17136];
            memset(buf, 0, sizeof(buf));

            printf("ret = read2(fh, buf, 17136) -> ");
            ret = read2(fh, buf, 17136);
            printf("returned %d\n", ret);

            printf("First 17136 bytes in file as string: \"%s\"\n", buf);

            memset(buf, 0, sizeof(buf));

            printf("Writing into file... return -> ");
            ret = write2(fh, "I NEVER CAME TO THE BEACH OR STOOD BY THE OCEAN", 47);
            printf("%d\n", ret);

            printf("ret = close2(fh) -> ");
            ret = close2(fh);
            printf("returned %d\n", ret);
            
            free(filepath);
        }
        else if (dentry.fileType == TYPEVAL_DIRETORIO) {
            char* dirpath;
            int subdir_name_length = strlen(dentry.name);
            int fullpath_length = path_length + subdir_name_length;
            
            dirpath = malloc(fullpath_length * sizeof(char));
            
            strcpy(dirpath, path);
            strcat(dirpath, dentry.name);
            
            printf("dh = opendir2(dirpath) -> ");
            DIR2 dh2 = opendir2(dirpath);
            printf("returned %d\n", dh2);
            
            read_dir(dh2, dirpath, fullpath_length);
            
            printf("ret = closedir2(dh2) -> ");
            ret = closedir2(dh2);
            printf("returned %d\n", ret);

            free(dirpath);         
        }
	} while (dentry.fileType != TYPEVAL_INVALIDO);    
}

int main() {
	int ret;
	DIR2 dh;

	printf("Beginning tests...\n");

	printf("dh = opendir2(\"/\") -> ");
	dh = opendir2("/");
	printf("returned %d\n", dh);

	read_dir(dh, "/", strlen("/")); 

	printf("ret = closedir2(dh) -> ");
	ret = closedir2(dh);
	printf("returned %d\n", ret);

	printf("Tests done!\n");

    return 0;
}
