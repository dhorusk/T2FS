#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "t2fs_helper.h"
#include "apidisk.h"

int main() {
	printf("opendir / returned: %d\n", opendir2("/"));
	printf("opendir /wrong returned: %d\n", opendir2("/wrong"));
	dumpOpenDirs();

	printf("open /file1 returned: %d\n", open2("/file1"));
	printf("open /file2 returned: %d\n", open2("/file2"));
	printf("open /file3 returned: %d\n", open2("/file3"));
	dumpOpenFiles();

    return 0;
}
