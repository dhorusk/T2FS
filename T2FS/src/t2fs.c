#include <string.h>
#include <math.h>
#include "t2fs.h"
#include "t2fs_helper.h"
#include "apidisk.h"
#include <stdio.h>

/*-----------------------------------------------------------------------------
Fun��o: Usada para identificar os desenvolvedores do T2FS.
	Essa fun��o copia um string de identifica��o para o ponteiro indicado por "name".
	Essa c�pia n�o pode exceder o tamanho do buffer, informado pelo par�metro "size".
	O string deve ser formado apenas por caracteres ASCII (Valores entre 0x20 e 0x7A) e terminado por �\0�.
	O string deve conter o nome e n�mero do cart�o dos participantes do grupo.

Entra:	name -> buffer onde colocar o string de identifica��o.
	size -> tamanho do buffer "name" (n�mero m�ximo de bytes a serem copiados).

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna "0" (zero).
	Em caso de erro, ser� retornado um valor diferente de zero.

Respons�vel: J�ssica
-----------------------------------------------------------------------------*/
int identify2 (char *name, int size) {
    if (!g_lib_init)
		initLibrary();

    if (size < 0)
        return -1;

    const char identify[] = "Daniel Kelling Brum - 233059\nJessica Salvador Rodrigues da Rocha - 242294\nMurilo Wolfart - 242226";

    memset(name, 0, size);
    memcpy(name, identify, size);

	return 0;
}


/*-----------------------------------------------------------------------------
Fun��o: Criar um novo arquivo.
	O nome desse novo arquivo � aquele informado pelo par�metro "filename".
	O contador de posi��o do arquivo (current pointer) deve ser colocado na posi��o zero.
	Caso j� exista um arquivo ou diret�rio com o mesmo nome, a fun��o dever� retornar um erro de cria��o.
	A fun��o deve retornar o identificador (handle) do arquivo.
	Esse handle ser� usado em chamadas posteriores do sistema de arquivo para fins de manipula��o do arquivo criado.

Entra:	filename -> path absoluto para o arquivo a ser criado. Todo o "path" deve existir.

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna o handle do arquivo (n�mero positivo).
	Em caso de erro, deve ser retornado um valor negativo.

Respons�vel: J�ssica / Murilo / Daniel
-----------------------------------------------------------------------------*/
FILE2 create2 (char *filename) {
	if (!g_lib_init)
		initLibrary();

	// split path and dir name
	char dir_path[500] = {0};
	char dir_name[MAX_FILE_NAME_SIZE];
	char *last_slash = strrchr(filename, '/');
	strncpy(dir_path, filename, last_slash - filename + 1);
	strcpy(dir_name, last_slash + 1);

	if (strlen(dir_name) > MAX_FILE_NAME_SIZE)
	   return -1;

	RECORD last_dir = getLastDirRecordFromPath(dir_path);	
	if (last_dir.TypeVal != TYPEVAL_DIRETORIO)
		return -2;

	FILE2 i = findFirstFreeFileHandle();  
	if (i == -1)
		return -3;

	RECORD file;
	file.TypeVal = 1;
	strcpy(file.name,dir_name);
	file.bytesFileSize = 0;
	file.blocksFileSize = 1;
	file.MFTNumber = getFirstFreeMftNumber();

    allocateMftTupla(file.MFTNumber);
    int offset = findFirstFreeDiskOffsetForRecordInDirRecord(last_dir);
	writeRecordToDiskOffset(&file, offset);

	return openFileUsingDirRecord(file, filename, offset);
}


/*-----------------------------------------------------------------------------
Fun��o:	Apagar um arquivo do disco.
	O nome do arquivo a ser apagado � aquele informado pelo par�metro "filename".

Entra:	filename -> nome do arquivo a ser apagado.

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna "0" (zero).
	Em caso de erro, ser� retornado um valor diferente de zero.

Respons�vel: Daniel
-----------------------------------------------------------------------------*/
int delete2 (char *filename) {
	if (!g_lib_init)
		initLibrary();

	if (filename == NULL)
		return -1;

	if (filename[0] != '/')
		return -2;

	if (isFileOpenByFullPath(filename))
		return -3;

	// split path and file name
	char file_path[500] = {0};
	char file_name[MAX_FILE_NAME_SIZE];
	char *last_slash = strrchr(filename, '/');
	strncpy(file_path, filename, last_slash - filename + 1);
	strcpy(file_name, last_slash + 1);

	// get the record for the last dir in the path
	RECORD last_dir = getLastDirRecordFromPath(file_path);
	if (last_dir.TypeVal != TYPEVAL_DIRETORIO)
		return -4;

	// if it's valid, find the file in it
	MFT_REGISTER last_dir_register = mftRegisterFromMftNumber(last_dir.MFTNumber);
	unsigned int disk_offset = fileDiskOffsetFromMftRegisterByName(last_dir_register, file_name);
    RECORD file = readRecordFromDiskOffset(disk_offset);
	if (file.TypeVal != TYPEVAL_REGULAR)
		return -5;

	// update file record
	file.TypeVal = TYPEVAL_INVALIDO;
	writeRecordToDiskOffset(&file, disk_offset);

	// invalidate all tuplas and free all blocks
	freeAllBlocksAndTuplasInRegisterByVBN(file.MFTNumber, 0, false);

    return 0;
}


/*-----------------------------------------------------------------------------
Fun��o:	Abre um arquivo existente no disco.
	O nome desse novo arquivo � aquele informado pelo par�metro "filename".
	Ao abrir um arquivo, o contador de posi��o do arquivo (current pointer) deve ser colocado na posi��o zero.
	A fun��o deve retornar o identificador (handle) do arquivo.
	Esse handle ser� usado em chamadas posteriores do sistema de arquivo para fins de manipula��o do arquivo criado.
	Todos os arquivos abertos por esta chamada s�o abertos em leitura e em escrita.
	O ponto em que a leitura, ou escrita, ser� realizada � fornecido pelo valor current_pointer (ver fun��o seek2).

Entra:	filename -> nome do arquivo a ser apagado.

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna o handle do arquivo (n�mero positivo)
	Em caso de erro, deve ser retornado um valor negativo

Respons�vel: Daniel
-----------------------------------------------------------------------------*/
FILE2 open2 (char *filename) {
	if (!g_lib_init)
		initLibrary();

	if (filename == NULL)
		return -1;

	if (filename[0] != '/')
		return -2;

	if (isFileOpenByFullPath(filename))
		return -3;

	// split path and file name
	char file_path[500] = {0};
	char file_name[MAX_FILE_NAME_SIZE];
	char *last_slash = strrchr(filename, '/');
	strncpy(file_path, filename, last_slash - filename + 1);
	strcpy(file_name, last_slash + 1);

	// get the record for the last dir in the path
	RECORD last_dir = getLastDirRecordFromPath(file_path);
	if (last_dir.TypeVal != TYPEVAL_DIRETORIO)
		return -4;

	// if it's valid, find the file in it
	MFT_REGISTER last_dir_register = mftRegisterFromMftNumber(last_dir.MFTNumber);
	unsigned int disk_offset = fileDiskOffsetFromMftRegisterByName(last_dir_register, file_name);
    RECORD file = readRecordFromDiskOffset(disk_offset);
	if (file.TypeVal != TYPEVAL_REGULAR)
		return -5;

	return openFileUsingDirRecord(file, filename, disk_offset);
}


/*-----------------------------------------------------------------------------
Fun��o:	Fecha o arquivo identificado pelo par�metro "handle".

Entra:	handle -> identificador do arquivo a ser fechado

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna "0" (zero).
	Em caso de erro, ser� retornado um valor diferente de zero.

Respons�vel: Murilo
-----------------------------------------------------------------------------*/
int close2 (FILE2 handle) {
	if (!g_lib_init)
		initLibrary();

	if (!isFileOpenByHandle(handle))
        return -1;
    removeFromOpenFiles(handle);
    return 0;
}


/*-----------------------------------------------------------------------------
Fun��o:	Realiza a leitura de "size" bytes do arquivo identificado por "handle".
	Os bytes lidos s�o colocados na �rea apontada por "buffer".
	Ap�s a leitura, o contador de posi��o (current pointer) deve ser ajustado para o byte seguinte ao �ltimo lido.

Entra:	handle -> identificador do arquivo a ser lido
	buffer -> buffer onde colocar os bytes lidos do arquivo
	size -> n�mero de bytes a serem lidos

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna o n�mero de bytes lidos.
	Se o valor retornado for menor do que "size", ent�o o contador de posi��o atingiu o final do arquivo.
	Em caso de erro, ser� retornado um valor negativo indicando o erro.
        -1 : arquivo fechado
        -2 : erro na leitura do setor

Respons�vel: Murilo
-----------------------------------------------------------------------------*/
int read2 (FILE2 handle, char *buffer, int size) {
	if (!g_lib_init)
		initLibrary();

    if (!isFileOpenByHandle(handle))
        return -1;

    // INITS: START
    FILE_DESCRIPTOR *file = &g_open_files[handle];

    WORD block_size = g_boot_block.blockSize;                               //Size of a block (in sectors)
    DWORD block_size_bytes = block_size * SECTOR_SIZE;                      //Size of a block (in bytes)
    DWORD VBN_number = floor((file->current_position) / block_size_bytes);  //VBN where this byte is located

    MFT_REGISTER file_mft = mftRegisterFromMftNumber(file->mft_number);     //MFT Register for the file
    int start_4tupla = getMftTuplaIndexUsingVBN(VBN_number, file_mft);             //4Tupla in the Register to start the sweeping

    DWORD LBN_number = file_mft.mft_tuplas[start_4tupla].logicalBlockNumber + (VBN_number - file_mft.mft_tuplas[start_4tupla].virtualBlockNumber);
    unsigned int start_sector = LBN_number * block_size;                    //Sector to start the sweeping

    int num_of_sectors = (int)ceil((double)size/SECTOR_SIZE);               //number of sectors to be read    
    int swept_sectors = 0;                                                  //number of read "swept" sectors
    int swept_4tupla_sectors = 0;                                           //number of read "swept" sectors in one 4tupla
    unsigned char sector_buffer[SECTOR_SIZE];                               //buffer for a sector

    int current_4tupla = start_4tupla;                                      //init current_4tupla
    unsigned int current_sector;                                            //init current_sector
    char* buffer_position_target;                                           //variable used to find where in the buffer to store the read sector buffer
    
    char* buffer_total_sectors = malloc(num_of_sectors * SECTOR_SIZE * sizeof(char));   //variable used to store all read sectors
        
    int starting_byte = (file->current_position) % block_size_bytes;        //starting byte in the buffer
    // INITS: END

    // Save sectors in the buffer.
    while(swept_sectors < num_of_sectors) {
        current_sector = start_sector + swept_4tupla_sectors;

        // If the MFT 4tupla has reached its end, go to the next 4tupla.
        if(is4tuplaEnded(file_mft.mft_tuplas[current_4tupla], swept_4tupla_sectors)) {
            current_4tupla++;
            
            //if the next is the final tupla, end
            if(file_mft.mft_tuplas[current_4tupla].atributeType == 0)
                break;
            else if(file_mft.mft_tuplas[current_4tupla].atributeType == 2) {
                file_mft = mftRegisterFromMftNumber(file_mft.mft_tuplas[current_4tupla].virtualBlockNumber);
                current_4tupla = 0;
            }

            LBN_number = file_mft.mft_tuplas[current_4tupla].logicalBlockNumber;
            start_sector = (block_size * LBN_number);

            swept_4tupla_sectors = 0;
            continue;
        }
            
        // If there was an error while reading, return.
        if(read_sector(current_sector, sector_buffer))
            return -2;
        
        // Save bytes in the main buffer.
        buffer_position_target = buffer_total_sectors + (swept_sectors * SECTOR_SIZE);
        memcpy(buffer_position_target, (char*)sector_buffer, SECTOR_SIZE);

        swept_4tupla_sectors++;
        swept_sectors++;
    }
    
    //copy only the desired bytes
    memcpy(buffer, buffer_total_sectors + starting_byte, size);
    
    int old_position = file->current_position;
    file->current_position += size;
    return fmin(size, file->file_size - old_position);

}


/*-----------------------------------------------------------------------------
Fun��o:	Realiza a escrita de "size" bytes no arquivo identificado por "handle".
	Os bytes a serem escritos est�o na �rea apontada por "buffer".
	Ap�s a escrita, o contador de posi��o (current pointer) deve ser ajustado para o byte seguinte ao �ltimo escrito.

Entra:	handle -> identificador do arquivo a ser escrito
	buffer -> buffer de onde pegar os bytes a serem escritos no arquivo
	size -> n�mero de bytes a serem escritos

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna o n�mero de bytes efetivamente escritos.
	Em caso de erro, ser� retornado um valor negativo.
		-1 : Arquivo fechado
		-2 : Mem�ria cheia

Respons�vel: Murilo
-----------------------------------------------------------------------------*/
int write2 (FILE2 handle, char *buffer, int size) {
	if (!g_lib_init)
		initLibrary();

	// if file is not open, return with error
	if (!isFileOpenByHandle(handle))
        return -1;

    // phase 1: get the LBN where we should write the data (won't modify anything)

    // size of a block in the system
    unsigned int block_size_in_bytes = g_boot_block.blockSize * SECTOR_SIZE;

    FILE_DESCRIPTOR open_file = g_open_files[handle];

    // calculate VBN where the current pointer is, then calculate tupla and get the LBN
    unsigned int current_VBN = floor(open_file.current_position / block_size_in_bytes);
    int current_tupla_index = getMftTuplaIndexUsingVBN(current_VBN, mftRegisterFromMftNumber(open_file.mft_number));

    // phase 1 completed: now we have the LBN where we should start writing the bytes.
    // phase 2: calculate how much space we need. MFT is modified here.

    MFT_REGISTER current_reg = mftRegisterFromMftNumber(open_file.mft_number);


	int amount_of_new_blocks_needed;	

	if (open_file.current_position + size <= open_file.file_size)
		amount_of_new_blocks_needed = 0;
	else
		amount_of_new_blocks_needed = (int)ceil((open_file.current_position + size - open_file.file_size)/BLOCK_SIZE);

	int allocation_result = allocateBlocksInMft(current_reg, open_file.mft_number, amount_of_new_blocks_needed, (int)ceil((float)open_file.file_size / (float)BLOCK_SIZE));

	if (allocation_result == -1)
		// error in allocation -> no space
		return -2;
	
	// update
	current_reg = mftRegisterFromMftNumber(open_file.mft_number);

	// phase 2 completed: if it was successful, now we have enough space for the data to write. The MFT isn't modified from this point on anymore.
	// phase 3: write data and fix record

	DWORD current_byte = open_file.current_position % block_size_in_bytes;

	if (amount_of_new_blocks_needed > 0)
		writeDataUsingMFT(current_reg, current_tupla_index, current_VBN, current_byte, buffer, size, true);
	else
		writeDataUsingMFT(current_reg, current_tupla_index, current_VBN, current_byte, buffer, size, false);

	RECORD file_record = readRecordFromDiskOffset(open_file.record_offset);

	if ((int)(size - (file_record.bytesFileSize - open_file.current_position)) > 0) {
		file_record.bytesFileSize += size - (file_record.bytesFileSize - open_file.current_position);
		file_record.blocksFileSize += amount_of_new_blocks_needed;
		writeRecordToDiskOffset(&file_record, open_file.record_offset);
		g_open_files[handle].file_size = file_record.bytesFileSize;
	}

	g_open_files[handle].current_position += size; 

	// phase 3 completed: return.

    return size;
}



/*-----------------------------------------------------------------------------
Fun��o:	Fun��o usada para truncar um arquivo.
	Remove do arquivo todos os bytes a partir da posi��o atual do contador de posi��o (CP)
	Todos os bytes a partir da posi��o CP (inclusive) ser�o removidos do arquivo.
	Ap�s a opera��o, o arquivo dever� contar com CP bytes e o ponteiro estar� no final do arquivo

Entra:	handle -> identificador do arquivo a ser truncado

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna "0" (zero).
	Em caso de erro, ser� retornado um valor diferente de zero.

Respons�vel: Daniel
-----------------------------------------------------------------------------*/
int truncate2 (FILE2 handle) {
	if (!g_lib_init)
		initLibrary();

    FILE_DESCRIPTOR *file = &g_open_files[handle];

	/* update file descriptor */
	file->file_size = file->current_position;

	/* update file record */
	RECORD record = readRecordFromDiskOffset(file->record_offset);
	record.bytesFileSize = file->file_size;
	record.blocksFileSize = file->file_size / BLOCK_SIZE;
	writeRecordToDiskOffset(&record, file->record_offset);

	/* invalidate all tuplas and free all blocks after the last needed one */
	unsigned int needed_VBNs = ceil((double) file->current_position / BLOCK_SIZE);
	freeAllBlocksAndTuplasInRegisterByVBN(file->mft_number, needed_VBNs, false);

	/* don't invalidate the entire register */
	if (needed_VBNs == 0)
		createMftTupla(file->mft_number);

	return 0;
}


/*-----------------------------------------------------------------------------
Fun��o:	Reposiciona o contador de posi��es (current pointer) do arquivo identificado por "handle".
	A nova posi��o � determinada pelo par�metro "offset".
	O par�metro "offset" corresponde ao deslocamento, em bytes, contados a partir do in�cio do arquivo.
	Se o valor de "offset" for "-1", o current_pointer dever� ser posicionado no byte seguinte ao final do arquivo,
		Isso � �til para permitir que novos dados sejam adicionados no final de um arquivo j� existente.

Entra:	handle -> identificador do arquivo a ser escrito
	offset -> deslocamento, em bytes, onde posicionar o "current pointer".

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna "0" (zero).
	Em caso de erro, ser� retornado um valor diferente de zero, relativo ao erro dado.
        -1 : arquivo fechado
        -2 : offset inv�lida

Respons�vel: Murilo
-----------------------------------------------------------------------------*/
int seek2 (FILE2 handle, DWORD offset) {
	if (!g_lib_init)
		initLibrary();

	bool file_closed = !isFileOpenByHandle(handle);

    if (file_closed)
        return -1;

    FILE_DESCRIPTOR *file = &g_open_files[handle];

    bool offset_in_file = ((offset >= 0) && (offset <= file->file_size));
    bool offset_end = (offset == -1);
    bool valid_offset = (offset_in_file || offset_end);

    if (!valid_offset)
        return -2;

    if (offset == -1)
        file->current_position = file->file_size;
    else
        file->current_position = offset;

    return 0;
}


/*-----------------------------------------------------------------------------
Fun��o:	Criar um novo diret�rio.
	O caminho desse novo diret�rio � aquele informado pelo par�metro "pathname".
		O caminho pode ser ser absoluto ou relativo.
	S�o considerados erros de cria��o quaisquer situa��es em que o diret�rio n�o possa ser criado.
		Isso inclui a exist�ncia de um arquivo ou diret�rio com o mesmo "pathname".

Entra:	pathname -> caminho do diret�rio a ser criado

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna "0" (zero).
	Em caso de erro, ser� retornado um valor diferente de zero.

Respons�vel: J�ssica / Daniel
-----------------------------------------------------------------------------*/
int mkdir2 (char *pathname) {
	if (!g_lib_init)
		initLibrary();

	if (pathname == NULL)
		return -1;

	if (pathname[0] != '/')
		return -2;

	// split path and dir name
	char dir_path[500] = {0};
	char dir_name[MAX_FILE_NAME_SIZE];
	char *last_slash = strrchr(pathname, '/');
	strncpy(dir_path, pathname, last_slash - pathname + 1);
	strcpy(dir_name, last_slash + 1);

	if (strlen(dir_name) > MAX_FILE_NAME_SIZE)
	   return -3;
 
	RECORD last_dir = getLastDirRecordFromPath(dir_path);	
	if (last_dir.TypeVal != TYPEVAL_DIRETORIO)
		return -4;

	RECORD dir;
	dir.TypeVal = 2;
	strcpy(dir.name, dir_name);
	dir.bytesFileSize = 0;
	dir.blocksFileSize = 1;
	dir.MFTNumber = getFirstFreeMftNumber();

    allocateMftTupla(dir.MFTNumber);
    int offset = findFirstFreeDiskOffsetForRecordInDirRecord(last_dir);
	writeRecordToDiskOffset(&dir, offset);

	return 0;
}


/*-----------------------------------------------------------------------------
Fun��o:	Apagar um subdiret�rio do disco.
	O caminho do diret�rio a ser apagado � aquele informado pelo par�metro "pathname".
	S�o considerados erros quaisquer situa��es que impe�am a opera��o.
		Isso inclui:
			(a) o diret�rio a ser removido n�o est� vazio;
			(b) "pathname" n�o existente;
			(c) algum dos componentes do "pathname" n�o existe (caminho inv�lido);
			(d) o "pathname" indicado n�o � um arquivo;

Entra:	pathname -> caminho do diret�rio a ser criado

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna "0" (zero).
	Em caso de erro, ser� retornado um valor diferente de zero.

Respons�vel: Daniel
-----------------------------------------------------------------------------*/
int rmdir2 (char *pathname) {
	if (!g_lib_init)
		initLibrary();

	if (pathname == NULL)
		return -1;

	if (pathname[0] != '/')
		return -2;

	if (isDirOpenByFullPath(pathname))
		return -3;

	// split path and dir name
	char dir_path[500] = {0};
	char dir_name[MAX_FILE_NAME_SIZE];
	char *last_slash = strrchr(pathname, '/');
	strncpy(dir_path, pathname, last_slash - pathname + 1);
	strcpy(dir_name, last_slash + 1);

	// get the record for the last dir in the path
	RECORD last_dir = getLastDirRecordFromPath(dir_path);
	if (last_dir.TypeVal != TYPEVAL_DIRETORIO)
		return -4;

	// if it's valid, find the dir in it
	MFT_REGISTER last_dir_register = mftRegisterFromMftNumber(last_dir.MFTNumber);
    unsigned int disk_offset = dirDiskOffsetFromMftRegisterByName(last_dir_register, dir_name);
    RECORD dir = readRecordFromDiskOffset(disk_offset);
	if (dir.TypeVal != TYPEVAL_DIRETORIO)
		return -5;

	// if it's not empty, fail
	if (!isDirEmpty(dir))
		return -6;

	// update dir record
	dir.TypeVal = TYPEVAL_INVALIDO;
	writeRecordToDiskOffset(&dir, disk_offset);

	// invalidate all tuplas and free all blocks
	freeAllBlocksAndTuplasInRegisterByVBN(dir.MFTNumber, 0, false);

	return 0;
}


/*-----------------------------------------------------------------------------
Fun��o:	Abre um diret�rio existente no disco.
	O caminho desse diret�rio � aquele informado pelo par�metro "pathname".
	Se a opera��o foi realizada com sucesso, a fun��o:
		(a) deve retornar o identificador (handle) do diret�rio
		(b) deve posicionar o ponteiro de entradas (current entry) na primeira posi��o v�lida do diret�rio "pathname".
	O handle retornado ser� usado em chamadas posteriores do sistema de arquivo para fins de manipula��o do diret�rio.

Entra:	pathname -> caminho do diret�rio a ser aberto

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna o identificador do diret�rio (handle).
	Em caso de erro, ser� retornado um valor negativo.

Respons�vel: Daniel
-----------------------------------------------------------------------------*/
DIR2 opendir2 (char *pathname) {
	if (!g_lib_init)
		initLibrary();

	if (pathname == NULL)
		return -1;

	if (strcmp(pathname, "/") == 0)
		return openRootDir();

	if (pathname[0] != '/')
		return -2;

	if (isDirOpenByFullPath(pathname))
		return -3;

	// split path and dir name
	char dir_path[500] = {0};
	char dir_name[MAX_FILE_NAME_SIZE];
	char *last_slash = strrchr(pathname, '/');
	strncpy(dir_path, pathname, last_slash - pathname + 1);
	strcpy(dir_name, last_slash + 1);

	// get the record for the last dir in the path
	RECORD last_dir = getLastDirRecordFromPath(dir_path);
	if (last_dir.TypeVal != TYPEVAL_DIRETORIO)
		return -4;

	// if it's valid, find the dir in it
	MFT_REGISTER last_dir_register = mftRegisterFromMftNumber(last_dir.MFTNumber);
    unsigned int dir_offset = dirDiskOffsetFromMftRegisterByName(last_dir_register, dir_name);
    RECORD dir = readRecordFromDiskOffset(dir_offset);
	if (dir.TypeVal != TYPEVAL_DIRETORIO)
		return -5;

	return openDirUsingDirRecord(dir, pathname, dir_offset);
}


/*-----------------------------------------------------------------------------
Fun��o:	Realiza a leitura das entradas do diret�rio identificado por "handle".
	A cada chamada da fun��o � lida a entrada seguinte do diret�rio representado pelo identificador "handle".
	Algumas das informa��es dessas entradas devem ser colocadas no par�metro "dentry".
	Ap�s realizada a leitura de uma entrada, o ponteiro de entradas (current entry) � ajustado para a pr�xima entrada v�lida
	S�o considerados erros:
		(a) t�rmino das entradas v�lidas do diret�rio identificado por "handle".
		(b) qualquer situa��o que impe�a a realiza��o da opera��o

Entra:	handle -> identificador do diret�rio cujas entradas deseja-se ler.
	dentry -> estrutura de dados onde a fun��o coloca as informa��es da entrada lida.

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna "0" (zero).
	Em caso de erro, ser� retornado um valor negativo
		Se o diret�rio chegou ao final, retorna "-END_OF_DIR" (-1)
		Outros erros, ser� retornado um outro valor negativo

Respons�vel: J�ssica / Daniel
-----------------------------------------------------------------------------*/
int readdir2 (DIR2 handle, DIRENT2 *dentry) {
    if (!g_lib_init)
        initLibrary();

    if (!isDirOpenByHandle(handle))
		return -2;

	DIR_DESCRIPTOR *dir = &g_open_dirs[handle];

	// if we're past the end of the dir
    if (dir->current_position >= dir->dir_size)
        return -1;

    MFT_REGISTER mft_reg = mftRegisterFromMftNumber(dir->mft_number);

	// read 4tupla with this VBN
    int VBN = dir->current_position * sizeof(RECORD) / BLOCK_SIZE;
    int LBN = findLBNInRegisterWithVBN(VBN, mft_reg);

	// read block
    unsigned char *block_data = calloc(BLOCK_SIZE, sizeof(char));
    readBlockByLBN(block_data, LBN);

	// read record
    RECORD record;
    int record_offset = dir->current_position * sizeof(RECORD) % BLOCK_SIZE;
    memcpy(&record, block_data + record_offset, sizeof(RECORD));

	if (record.TypeVal == TYPEVAL_INVALIDO) {
		strcpy(dentry->name, "");
		dentry->fileType = TYPEVAL_INVALIDO;
		dentry->fileSize = 0;
		return -3;
	}

    // fill dentry with infos from record
    fillDirEntryFromRecord(&record, dentry);

	// adjust current_position
	dir->current_position += 1;

	free(block_data);
    return 0;
}


/*-----------------------------------------------------------------------------
Fun��o:	Fecha o diret�rio identificado pelo par�metro "handle".

Entra:	handle -> identificador do diret�rio que se deseja fechar (encerrar a opera��o).

Sa�da:	Se a opera��o foi realizada com sucesso, a fun��o retorna "0" (zero).
	Em caso de erro, ser� retornado um valor diferente de zero.

Respons�vel: Murilo
-----------------------------------------------------------------------------*/
int closedir2 (DIR2 handle) {
	if (!g_lib_init)
		initLibrary();

	if (!isDirOpenByHandle(handle))
        return -1;

    removeFromOpenDirs(handle);
    return 0;
}
